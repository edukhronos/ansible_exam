# create database
drop database if exists neo01;
create database neo01 default character set utf8;

# create table
use neo01;
drop table if exists users;

create table users (
    user_id     int         unsigned not null,
    nickname    varchar(20)          not null,
    password    char(64)             not null,
    first_name  varchar(20)          not null,
    last_name   varchar(20)          not null,
    email       varchar(256)         not null,
    updated_at  datetime             not null
)engine = InnoDB;

alter table users
    add primary key (user_id);

insert into users values
(1,  'user01', 'aaa', 'ichiro',  'yamada',     'ichiro@example.com',  '2019-01-01 00:00:00'),
(2,  'user02', 'aaa', 'jiro',    'watanabe',   'jiro@example.com',    '2019-01-01 00:00:00'),
(3,  'user03', 'aaa', 'saburo',  'suzuki',     'saburo@example.com',  '2019-01-01 00:00:00'),
(4,  'user04', 'aaa', 'shiro',   'takahashi',  'shiro@example.com',   '2019-01-01 00:00:00'),
(5,  'user05', 'aaa', 'goro',    'inogashira', 'goro@example.com',    '2019-01-01 00:00:00'),
(6,  'user06', 'aaa', 'rokuro',  'sakai',      'rokuro@example.com',  '2019-01-01 00:00:00'),
(7,  'user07', 'aaa', 'nanaro',  'kato',       'nanaro@example.com',  '2019-01-01 00:00:00'),
(8,  'user08', 'aaa', 'hachiro', 'ueda',       'hachiro@example.com', '2019-01-01 00:00:00'),
(9,  'user09', 'aaa', 'kuro',    'shirota',    'kuro@example.com',    '2019-01-01 00:00:00'),
(10, 'user10', 'aaa', 'juro',    'tanabe',     'juro@example.com',    '2019-01-01 00:00:00');
